#Ansible | Provisionner et configurer une instance ec2

_______


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

> **Carlin FONGANG**  | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Exécuter ce playbook

1. Requirement.yml

Exporter les info d'auth Gitlab pour la gestion du remote backend

```bash
export GITLAB_ACCESS_TOKEN='glpat-TMnaaBhdgF38wQx4PstE'
export TF_STATE_NAME='odoo_state'
export PROJECT_ID='57006050' ##ID du depot terraform qui provisionne les instance
```

Modifier l'utilisateur Gitlab dans le fichier **group_vars/all.yml**
**>>-backend-config="username=CarlinFongang"<<**
mettre le bon nom d'utilisateur

2. AWS
exporter les info d'auth AWS

````bash
export AWS_ACCESS_KEY_ID='AKIA6ODU3U2MDBB7WRPT'
export AWS_SECRET_ACCESS_KEY='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'
````

3. Exécution du playbook

````bash
ansible-galaxy install -r roles/requirements.yml -f
````

![alt text](image.png)


````bash
ansible-playbook -i hosts.yml deploy_conf_ec2.yml -K -vvv
````


Dépôt du projet utilisant les roles en remote localement :  [Roles Distant](https://gitlab.com/CarlinFongang-Labs/Ansible/ansible-project/remote-role.git)


````bash
cd /mnt/c/Users/Kusuka.fr/OneDrive - data5tb/Formations/BootCamp DevOps 17/cursus-devops/Odoo Projects/odoo-et-pgadmin/provide_conf_compute
````