# CI/CD | Mise en œuvre d'un Pipeline CI/CD avec Jenkins

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte : IC GROUP et son projet de site vitrine

Une entreprise nommée IC GROUP, souhaite mettre en place un site vitrine pour fournir un accès à ses deux applications phares :

* **Odoo** et 
* **pgAdmin**

**Odoo** est un ERP polyvalent qui permet de gérer les ventes, les achats, la comptabilité, les stocks, le personnel, etc.

Odoo est distribué en versions communautaires et entreprise. Souhaitant un contrôle accru sur le code et la possibilité de réaliser des modifications et personnalisations, IC GROUP a opté pour l'édition communautaire. Plusieurs versions d'Odoo sont disponibles, et le choix s'est porté sur la version 13.0 car elle intègre un Learning Management System (LMS) qui servira à la publication de formations internes et à la diffusion de l'information.

Liens utiles :

Site officiel : [https://www.odoo.com/](https://www.odoo.com/)
GitHub officiel : [https://github.com/odoo/odoo.git](https://github.com/odoo/odoo.git)
Docker Hub officiel : [https://hub.docker.com/_/odoo](https://hub.docker.com/_/odoo)

**pgAdmin**, quant à lui, permettra l'administration graphique de la base de données PostgreSQL créée précédemment.

Site officiel : [https://www.pgadmin.org/](https://www.pgadmin.org/)
Docker Hub officiel : [https://hub.docker.com/r/dpage/pgadmin4/](https://hub.docker.com/r/dpage/pgadmin4/)

Le site vitrine a été conçu par l'équipe de développement de l'entreprise, et les fichiers associés se trouvent dans le dépôt suivant : [Dépot site vitrine](https://github.com/eazytraining/projet-fils-rouge.git). 

Notre mission consiste à conteneuriser cette application tout en permettant la configuration des URLs des applications (Odoo et pgAdmin) via des variables d'environnement.




## Prérequis 
 - Il est nécessaire de disposer d'une instance ou d'un serveur avec une adresse IP publique. 
    [Provisionner une instance ec2 avec terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/terraform-provide-ec2-instance.git)

 - Modifiez les règles entrantes et sortantes du groupe de sécurité associé à l'instance pour autoriser le trafic TCP. Cette configuration est requise pour nos requêtes curl ainsi que pour les protocoles HTTP et HTTPS, qui facilitent l'accès aux services web. Il est également essentiel d'autoriser le protocole SSH afin de permettre la connexion à l'hôte pendant les opérations de déploiement de l'application.


Pour une présentation plus claire et plus professionnelle sur l'automatisation du déploiement dans votre pipeline Jenkins en utilisant Docker Compose et Ansible, voici une rédaction améliorée :

---

### Automatisation du déploiement avec Jenkins, Docker Compose et Ansible

Pour optimiser le déploiement de nos applications dans notre pipeline Jenkins, nous intégrons l'Infrastructure as Code (IaC) via Docker Compose et Ansible. Cette approche vise à faciliter la gestion et le déploiement automatisé des applications Odoo et pgadmin.

#### 1. Déploiement de l'application Odoo avec Docker Compose

  **1.1. Description du fichier `odoo-compose.yml`**

Nous allons tout dabord créer un fichier nommé **odoo-compose.yml** dans le quel nous allons décrire le process de depliement de l'application Odoo ainsi que de la base de données et volume persistant

```bash
nano .env
```

```yaml
# General

# Odoo variables
ODOO_IMAGE=odoo:16.0
ODOO_EXPOSE_PORT=8080
## volumes
ODOO_CONFIG_PATH=./config
ODOO_ADDONS_PATH=./addons
ODOO_WEB_DATA=odoo-web-data
#ODOO_DB_PATH=./db-data
## Pass
ODOO_PASSWORD_FILE=/run/secrets/postgresql_password
#POSTGRESQL_PASSWORD=postgresql_password

# Postgresql bd
POSTGRES_IMAGE=postgres:15
POSTGRES_DB=postgres
POSTGRES_PASSWORD_FILE=/run/secrets/postgresql_password
POSTGRES_USER=odoo

# pgAdmin variables
PGADMIN_IMAGE=dpage/pgadmin4
PGADMIN_EMAIL=fongangcarlin@gmail.com
PGADMIN_PASSWORD=admin
EXPOSE_PORT_PGADMIN=5050
PGADMIN_VOLUME_NAME=pgadmin-data
```

```bash
set -a
source .env
set +a
```



```bash
nano odoo-compose.yml
```

```yaml
version: '3.7'

services:
  odoo_web_service:
    image: ${ODOO_IMAGE}
    depends_on:
      - db
    ports:
      - "${ODOO_EXPOSE_PORT}:8069"
    volumes:
      - "${ODOO_WEB_DATA}:/var/lib/odoo"
      - "${ODOO_CONFIG_PATH}:/etc/odoo"
      - "${ODOO_ADDONS_PATH}:/mnt/extra-addons"
    networks:
      - odoo_network
    environment:
      - PASSWORD_FILE=${ODOO_PASSWORD_FILE}
    secrets:
      - postgresql_password

  db:
    image: ${POSTGRES_IMAGE}
    environment:
      - POSTGRES_DB=${POSTGRES_DB}
      - POSTGRES_PASSWORD_FILE=${POSTGRES_PASSWORD_FILE}
      - POSTGRES_USER=${POSTGRES_USER}
      - PGDATA=/var/lib/postgresql/data/pgdata
    volumes:
      - "odoo-db-data:/var/lib/postgresql/data/pgdata"
    secrets:
      - postgresql_password
    networks:
      - odoo_network

volumes:
  odoo-web-data:
  odoo-db-data:

networks:
  odoo_network:

secrets:
  postgresql_password:
    file: ./.secrets/odoo_pg_pass
```

```bash
mkdir .secrets
```

````bash
echo "odoo" > .secrets/odoo_pg_pass
````

```bash
docker compose -f odoo-compose.yml up -d
```

![alt text](image.png)

Nous utilisons Docker-Compose pour orchestrer deux services principaux, dont  **odoo** et **db** qui s'appui sur le sgbd PostgreSQL.

**Service PostgreSQL (db) :**

**Image docker:** Nous utilisons postgres:13, une version stable de PostgreSQL.
**Variables d'environnement :** Configuration du nom de la base de données, de l'utilisateur, et du mot de passe, essentiels pour les connexions sécurisées.
**Volume :** Un volume odoo-db-data est défini pour persister les données de la base de données au-delà de la vie du conteneur, ce qui est crucial pour la sauvegarde et la restauration.
**Réseau :** Inclus dans le réseau odoo-network pour permettre une communication fluide avec le service Odoo.

**Service Odoo (odoo) :**

**Image docker :** odoo:14.0
**Dépendances :** Dépend du service **db** déclaré avant, ce qui garantis que la base de données est prête avant le démarrage d'Odoo.
**Ports :** Le port 8069 est exposé pour accéder à Odoo via un navigateur web.
**Variables d'environnement :** Définit les variables nécessaires pour la connexion à la base de données PostgreSQL.
**Volume :** Un volume odoo-web-data pour la persistance des fichiers de session et autres données importantes d'Odoo.
**Réseau :** Les services **"Docker"** et **"PostgreSQL"** sont connectés au même réseau pour une communication inter-services sans interruption.


**1.2. Test du fichier `odoo-compose.yml`**

![alt text](image.png)


![alt text](image-1.png)




#### 2. Déploiement de l'application pgadmin avec Docker Compose
- **Objectif** : Configurer et déployer l'application pgadmin en utilisant les paramètres spécifiés précédemment.
- **Étapes** :
  - Utiliser un fichier `docker-compose.yml` distinct pour pgadmin, configuré selon les spécifications du `servers.json` et intégrant la persistance des données.

#### 3. Création de rôles Ansible pour la gestion automatisée
- **Objectif** : Transformer les configurations de Docker Compose en rôles Ansible modulables et réutilisables.
- **Étapes** :
  - **Création des rôles `odoo_role` et `pgadmin_role`** :
    - **Variabilisation des composants Docker** :
      - **Nom du réseau et du volume** : Permettre la personnalisation du nom du réseau et du volume dans Docker à travers des variables Ansible.
      - **Répertoire de montage pour le volume** : Offrir la possibilité de définir un chemin local alternatif pour la persistance des données de la BDD Odoo.
      - **Nom des services et des containers** : Configurer les noms des services et des conteneurs créés par Docker Compose via des variables, rendant chaque instance du déploiement unique et adaptée aux besoins spécifiques.

### Conclusion

L'intégration de Docker Compose et Ansible dans notre pipeline Jenkins non seulement rationalise le processus de déploiement mais garantit également une gestion efficace et flexible des applications critiques. Cette stratégie d'automatisation permet une mise en œuvre rapide et des ajustements faciles aux configurations selon les exigences évolutives de nos projets.

---

Cette formulation enrichie fournit un aperçu détaillé et structuré qui devrait convenir parfaitement à un article de blog technique, en clarifiant les objectifs, les étapes et les avantages de l'approche proposée.